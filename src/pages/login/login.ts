import { Component } from "@angular/core";
import { NavController, NavParams, App, LoadingController } from "ionic-angular";
import { FormBuilder, Validators } from "@angular/forms";
import { Storage } from "@ionic/storage";
import { HomePage } from "../home/home";

@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  public loginForm: any;
  public myFormValid: any = 'INVALID';
  public body_form_login = {
    email: '',
    password: '',
  }
  public menu: string = 'login';
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public app: App,
    public storage: Storage,
    public loadingCtrl: LoadingController
  ) {
    this.loginForm = this.formBuilder.group({
      email: [
        "",
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])
      ],
      password: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(6),
        ])
      ]
    })
  }
  ionViewDidLoad() {
    this.loginForm.statusChanges.subscribe((resp) => {
      this.myFormValid = resp
    })
  }


  doLogin() {
    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    })
    loading.present().then(() => {
      let data = {
        user: this.loginForm.value.email,
        todoList: []
      }
      this.storage.set('user', data.user);
      this.storage.set('toDoList', data.todoList);
      this.storage.set('login_status', true);
      this.navCtrl.setRoot(HomePage);
      loading.dismiss();
    })

  }

  validation_messages = {
    email: [
      { type: "required", message: "Email Cannot be Empty" },
      { type: 'pattern', message: 'Enter a valid email' }
    ],
    password: [
      { type: "required", message: "Password Cannot be Empty" },
      { type: "minlength", message: "Password must be at least 6 characters long" },
    ],
  };
}

