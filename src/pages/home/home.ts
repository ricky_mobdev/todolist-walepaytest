import { Component, Input, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public data: any = {};
  public todo: any = [];
  public inputTodo: any = ''
  public time: any;
  @ViewChild('input') myInput;
  constructor(public navCtrl: NavController, public storage: Storage) {
    this.storage.get('toDoList').then((resp) => {
      this.todo = resp
    })
    this.initTime();
  }

  initTime() {
    var amOrPm = (new Date().getHours() < 12) ? "AM" : "PM";
    var hour = (new Date().getHours() < 12) ? new Date().getHours() : new Date().getHours() - 12;
    this.time = hour + ':' + new Date().getMinutes() + ' ' +amOrPm;
  }

  addItem() {
    if (this.inputTodo && this.inputTodo != '') {
      this.todo.push({ name: this.inputTodo, checked: false })
      this.storage.set('toDoList', this.todo);
      this.initTime();
      this.inputTodo = '';
    }
  }

  check(e, item) {
    console.log(e, item);
  }

  removeItem(item) {
    var index = this.todo.indexOf(item, 0);
    if (index > -1) {
      this.todo.splice(index, 1);
    }
    this.initTime();
    this.storage.set('toDoList', this.todo);
  }

  setFocus() {
    this.myInput.setFocus();
  }

  signOut() {
    // this.storage.clear();
    this.navCtrl.setRoot(LoginPage);
  }

}
